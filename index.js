const countdown = document.getElementById("lga");
const logo = document.getElementById("hplogo");

const text = document.createElement("div");
text.innerHTML = "Countdown";
text.style.color = "red";
text.style.fontSize = "40px";

const timer = document.createElement("div");
timer.style.color = "red";
timer.style.fontSize = "20px";
timer.innerHTML = "";

logo.style.height = "50px";

countdown.prepend(text);
text.append(timer);

let seconds = 5;
setInterval(() => {
  timer.innerHTML = `${seconds} sec`;
  seconds--;
}, 1000);
text.appendChild(timer);

setTimeout(() => {
  window.location.href = "http://news.google.co.in";
}, 5000);
